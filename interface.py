
#Interface reconnassance Faciale avec OpenCV et Face_recognition 
#Pajak Alexandre 2021
import face_recognition
from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout, QPushButton, QHBoxLayout
from PyQt5.QtGui import QPixmap
import sys
import cv2
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread
import numpy as np

indexCap = 1
message = ""
class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def __init__(self):
        super().__init__()
        self._run_flag = True
        self._cap_flag = False
        self._onoff_flag = True

    def run(self):
        global message
        #chargement de la base de donnée depuis fichier image
        obama_image = face_recognition.load_image_file("images/Obama.jpg")
        obama_face_encoding = face_recognition.face_encodings(obama_image)[0]

        # correspondance visage connu&nom
        known_face_encodings = [
            obama_face_encoding,
        ]
        known_face_names = [
            "Obama",
        ]

        # variables pour detection
        face_locations = []
        face_encodings = []
        face_names = []
        process_this_frame = True
        tickmark=cv2.getTickCount()
        color_info=(0, 0, 255)
        while self._run_flag:
            # capture from web cam
            if self._onoff_flag == True :
                cap = cv2.VideoCapture(0)
                ret, cv_img = cap.read()

                # downScale
                small_frame = cv2.resize(cv_img, (0, 0), fx=0.25, fy=0.25)

                # convertion BGR (OpenCV) a RGB (face_recognition)
                rgb_small_frame = small_frame[:, :, ::-1]

                if process_this_frame:
                    # trouvé les visages
                    face_locations = face_recognition.face_locations(rgb_small_frame)
                    face_encodings = face_recognition.face_encodings(
                        rgb_small_frame, face_locations)

                    face_names = []
                    for face_encoding in face_encodings:
                        # comparaison avec visage connu
                        matches = face_recognition.compare_faces(
                            known_face_encodings, face_encoding)
                        name = "Inconnu "
                        face_distances = face_recognition.face_distance(
                            known_face_encodings, face_encoding)
                        best_match_index = np.argmin(face_distances)
                        if matches[best_match_index]:
                            name = known_face_names[best_match_index]

                        face_names.append(name)

                process_this_frame = not process_this_frame

                # affichage
                if len(face_names) == 0 : 
                    message = "RAS" 
                else :
                    for name in face_names:
                        if name == "Inconnu " :
                            message = "intrus"
                        else :
                            message = "Bonjour "+name+" !"
                #affichage infos
                latitude = 50.5
                longitude = 60.5
                position="POS: "+str(latitude)+"/"+str(longitude)
                vitesse= 10
                distance = 10
                fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
                cv2.putText(cv_img, position , (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, color_info, 2)
                cv2.putText(cv_img, "VIT: {:05.2f}".format(vitesse), (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, color_info, 2)
                cv2.putText(cv_img, "DIST: {:05.2f}".format(distance), (10, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, color_info, 2)
                cv2.putText(cv_img, "FPS: {:05.2f}".format(fps), (10, 120), cv2.FONT_HERSHEY_SIMPLEX, 1, color_info, 2)
                cv2.putText(cv_img, message, (10, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, color_info, 2)

                for (top, right, bottom, left), name in zip(face_locations, face_names):
                    # retour format initial
                    top *= 4
                    right *= 4
                    bottom *= 4
                    left *= 4

                    # carré rouge
                    cv2.rectangle(cv_img, (left, top), (right, bottom), (0, 0, 255), 2)

                    # nom
                    cv2.rectangle(cv_img, (left, bottom - 35),
                                (right, bottom), (0, 0, 255), cv2.FILLED)
                    font = cv2.FONT_HERSHEY_DUPLEX
                    cv2.putText(cv_img, name, (left + 6, bottom - 6),
                                font, 1.0, (255, 255, 255), 1)

                if ret:
                    self.change_pixmap_signal.emit(cv_img)

                if self._cap_flag:
                    global indexCap
                    capName = "capture_"+str(indexCap)+".png"
                    filePath = "./captures/" + capName
                    cv2.imwrite(str(filePath),cv_img)
                    indexCap = indexCap + 1
                    self._cap_flag = False
            else:
                cap = cv2.imread("images/Logo.png")
                self.change_pixmap_signal.emit(cap)
                
        # arret capture
        cap.release()

    def appui_bouton_cap(self):
        print("Capture")
        global message
        message = "Capture OK"

    def appui_bouton_ON(self):
        if self._onoff_flag == True :
            self._onoff_flag = False
            print("OFF")
        else : 
            self._onoff_flag = True
            print("ON")

    def stop(self):
        self._run_flag = False
        self.wait()




class App(QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("FaceRec Interface")
        self.disply_width = 640
        self.display_height = 480
        # create the label that holds the image
        self.image_label = QLabel(self)
        self.image_label.resize(self.disply_width, self.display_height)
        # creation du premier bouton
        self.bouton_Cap = QPushButton("Prendre une photo")
        self.bouton_Start_Video = QPushButton("ON/OFF")
        
        # create a horizontal box layout  
        hbox = QVBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(self.bouton_Start_Video)
        hbox.addWidget(self.bouton_Cap)
        # create a vertical box layout and add the two labels
        vbox = QHBoxLayout()
        vbox.addLayout(hbox)
        vbox.addWidget(self.image_label)
        # set the vbox layout as the widgets layout
        self.setLayout(vbox)

        # create the video capture thread
        self.thread = VideoThread()
        # connect its signal to the update_image slot
        self.thread.change_pixmap_signal.connect(self.update_image)
        # start the thread
        self.thread.start()
        self.bouton_Cap.clicked.connect(self.thread.appui_bouton_cap)
        self.bouton_Start_Video.clicked.connect(self.thread.appui_bouton_ON)

    def closeEvent(self, event):
        self.thread.stop()
        event.accept()


    @pyqtSlot(np.ndarray)
    def update_image(self, cv_img):
        """Updates the image_label with a new opencv image"""
        qt_img = self.convert_cv_qt(cv_img)
        self.image_label.setPixmap(qt_img)
    
    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_Qt_format.scaled(self.disply_width, self.display_height, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)
    
if __name__=="__main__":
    app = QApplication(sys.argv)
    a = App()
    a.show()
    sys.exit(app.exec_())